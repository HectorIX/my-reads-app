import React, {Component} from 'react'
import PropTypes from 'prop-types'

class Book extends Component {

    static propTypes = {
        books: PropTypes.array
    }

    render() {

        const book = this.props.book

        return(
            <div className="book">
                <li>
                    <div className="book-top">
                        <div 
                            className="book-cover" 
                            style={{ backgroundImage: `url("${book.imageLinks ? book.imageLinks.thumbnail : ''}")` }}
                        >
                        </div>
                        <div className="book-shelf-changer">
                            <select 
                                    value={this.props.book.shelf ? this.props.book.shelf : 'none'}
                                    onChange={(e)=>this.props.onChangeShelf(book, e.target.value)}
                            >
                                <option value="move" disabled>Move to...</option>
                                <option value="currentlyReading">Currently Reading</option>
                                <option value="wantToRead">Want to Read</option>
                                <option value="read">Read</option>
                                <option value="none">None</option>
                            </select>
                        </div>
                    </div>
                    <div className="book-title">{book.title ? book.title : ''}</div>
                    <div className="book-authors">{book.authors ? book.authors.join(', ') : ''}</div>
                </li>
            </div>
        )
    }
}

export default Book