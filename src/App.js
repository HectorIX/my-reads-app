import React, {Component} from 'react'
import {Route, Switch} from 'react-router-dom'

import * as BooksAPI from './BooksAPI'
import './App.css'

import Bookcase from './Bookcase'
import Search from './Search'
import Page404 from './Page404';

class BooksApp extends Component {

  state = {
    books: []
  }

  componentDidMount() {
    // When the Component get mount
    // retrieve all books from the database
    // in order to make them visible in the Homepage.
    this.retrieveBooks()
  }

  retrieveBooks = () => {
    // Get all books by using the API
    BooksAPI.getAll()
      .then((books) => { // and update the UI state
        this.setState( () => ({ 
          books: books
        }))
      })
  }

  changeShelf = (book, shelf) => {
    // Check whether the book exists
    //  in the current state. 
    // If it is not present, then add it. 
    if (!this.state.books.includes(book)) {
      this.state.books.push(book)
    }
    // Then update the state in the UI. 
    this.setState( (currentState) => ({
      books : currentState.books.map((b) => {
        // Iterate through all books
        // and update the book's shelf 
        // with the exception the user 
        // has chosen 'None' as a group.
        if (b.id === book.id) {
          // Update the shelf in the UI
          b.shelf = shelf
        } 
        return b
      })
    }))
    // And then update the book 
    // in the database through the API. 
    BooksAPI.update(book, shelf)
  }

  render() {
    return (
      <div className="app">
        <Switch>
          <Route exact path='/' render={ () => (
              <Bookcase 
                key='bookcase'
                books={this.state.books} 
                onChangeShelf={this.changeShelf}/> 
          )} />  
          <Route exact path='/search' render={ () => (
              <Search 
                key='search'
                books={this.state.books}
                onChangeShelf={this.changeShelf}
              />
          )} />
          <Route component={Page404}/>
        </Switch>
      </div>
    )
  }
}

export default BooksApp
