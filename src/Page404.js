import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class Page404 extends Component {
    render() {
        return (
            <div>
                <h1>404</h1>
                <h3>There in such page</h3>
                <Link to='/'>Return to Home</Link>
            </div>
        )
    }
}
    

export default Page404