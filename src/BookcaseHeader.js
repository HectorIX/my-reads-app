import React, {Component} from 'react'

class BookcaseHeader extends Component {
    render() {
        return(
            <div className="list-books-title">
                <h1>{this.props.bookcase_title}</h1>
            </div>
        )
    }
}

export default BookcaseHeader