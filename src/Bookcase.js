import React, {Component} from 'react' 
import PropTypes from 'prop-types'

import BookcaseHeader from './BookcaseHeader'
import BookSelves from './BookSelves'
import SearchButton from './SearchButton'

class Bookcase extends Component {

    static propTypes = {
        books: PropTypes.array.isRequired
    }

    render() {

        const books = this.props.books

        return(
            <div className="list-books">
                <BookcaseHeader 
                    bookcase_title='My Bookcase'
                />
                <BookSelves 
                    books={books} 
                    onChangeShelf={this.props.onChangeShelf.bind(this)}
                />
                <SearchButton />
            </div>
        )
    }
}

export default Bookcase