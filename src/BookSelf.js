import React, {Component} from 'react'
import Book from './Book'
import PropTypes from 'prop-types'

class BookSelf extends Component {

    static propTypes = {
        books: PropTypes.string
    }
    
    render() {

        const books = this.props.books_in_self

        return(
            <div className="bookshelf">
                <h2 className="bookshelf-title">
                    {this.props.self_name}
                </h2>
                <div className="bookshelf-books">
                    <ol className="books-grid">
                        {books.map( (book) => (
                            <Book 
                                key={book.id} 
                                book={book} 
                                onChangeShelf={this.props.onChangeShelf.bind(this)} 
                            />
                        ))}
                    </ol>
                </div>
            </div>
        )
    }
}

export default BookSelf