import React, {Component} from 'react' 
import BookSelf from './BookSelf'
import PropTypes from 'prop-types'

class BookSelves extends Component {

    static propTypes = {
        books: PropTypes.array.isRequired
    }

    render() {

        const books = this.props.books

        return(
            <div className="list-books-content">
                <BookSelf 
                    key='Currently Reading'
                    self_name='Currently Reading' 
                    books_in_self={ books.filter( (book) => (
                        book.shelf === "currentlyReading"
                    ))}
                    onChangeShelf={this.props.onChangeShelf.bind(this)}
                />
                <BookSelf 
                    key='Want to Read'
                    self_name='Want to Read'
                    books_in_self={books.filter( (book) => (
                        book.shelf === "wantToRead"
                    ))}
                    onChangeShelf={this.props.onChangeShelf.bind(this)}
                />
                <BookSelf 
                    key='Read'
                    self_name='Read'
                    books_in_self={books.filter( (book) => (
                        book.shelf === "read"
                    ))}
                    onChangeShelf={this.props.onChangeShelf.bind(this)}
                />
            </div>
        )
    }
}

export default BookSelves