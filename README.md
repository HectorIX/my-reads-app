# My Reads App
An application that allows user to classify a list of books in four main categories: "currently reading", "want to read", "read" and "none". 
The UI is composed by 3 shelfs each one entitled with the appropriate category name. The books tagged with "none" are not visible in the homepage. 
The user can add those books by searching them and assign them a new category. 


# Install and Run the App
*  Clone the repo: `git clone https://gitlab.com/HectorIX/my-reads-app.git`
*  Then change to the Project's directory: `cd my-reads-app`
*  Install all projects dependencies: `npm install`
*  Start the server: `npm start` or `yarn start`


# How the App looks like

### Homepage

The homepage consists of three shelves. The user is able to move the books from one self to anothe or completely remove the book from the homepage. 

![homepage](https://gitlab.com/uploads/-/system/personal_snippet/1798181/b7cafc68331502d497dc16bade123ec1/homepage.png)

### Search Page

In this case scenario user is searching for the word "art" and all relevant books that contain this term either in the title or in the autors are appearing in the screen. 
The user is able to send a book to one of the homepage's shelves. 

![homepage](https://gitlab.com/uploads/-/system/personal_snippet/1798183/d0ee1e826935b68679d1eb6cf19bdddb/searchpage.png)

# Note 
This is the first out of three Assessment Projects of Udacity's React Nanodegree Program. We have been given the API to retrieve the books from a database and the page styles and html code. 