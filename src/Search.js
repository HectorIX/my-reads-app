import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import * as BooksAPI from './BooksAPI'
import Book from './Book'
import {DebounceInput} from 'react-debounce-input';


class Search extends Component {
    
    state = {
        foundBooks: [],
        query: ''
    }

    searchBooks = (query) => {
        if (query) {
          BooksAPI.search(query)
            .then((books) => {
                this.setState({
                    foundBooks: books
                })
            })
        } else {
          this.setState({foundBooks: []})
        }
    }

    render() {
        return(
            <div className="search-books">
                <div className="search-books-bar">
                    <Link to='/' className='close-search'>
                        Close
                    </Link>
                    <div className="search-books-input-wrapper">
                        <DebounceInput
                            type="text" 
                            minLength={1}
                            debounceTimeout={500} 
                            placeholder="Search by title or author"
                            onChange={(e)=>(this.searchBooks(e.target.value.trim()))}
                        />
                    </div>
                </div>
                
                <div className="search-books-results">
                    { typeof this.state.foundBooks !== 'undefined' && this.state.foundBooks.length > 0 && (
                        <ol className="books-grid">
                            {this.state.foundBooks.map( (book) => (
                                <Book 
                                    key={book.id} 
                                    book={book} 
                                    onChangeShelf={this.props.onChangeShelf.bind(this)} 
                                />
                            ))}
                        </ol>
                    )}
                </div>
                
            </div>
        )
    }
}

export default Search